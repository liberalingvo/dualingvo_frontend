import time, selenium, json, os
import argparse
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
#ac = selenium.ActionChains(driver)

parser = argparse.ArgumentParser(description='Skrapas la duolingo vortaro.')
parser.add_argument('-c', '--cellingvo', type=str, help='la cellingvo')
parser.add_argument('-g', '--gepatralingvo', type=str, help='la fonta lingvo')
#Show More Sentences
parser.add_argument('-p', '--pli_da_frazoj', type=str,
    help='la surskribajxo de la buteno kiu montras pli da frazojn en la vortaro'
)
parser.add_argument('-a', '--cxiuj_vortoj_de_lingvo', action='store_true',
    help='Cxu cxiuj vortoj aux nur de la en-eo kurso'
)
args = parser.parse_args()
try:
    al, de, pli, cxiuj_vortoj = args.cellingvo, args.gepatralingvo, args.pli_da_frazoj, args.cxiuj_vortoj_de_lingvo
except Exception as e:
    parser.print_help()
    exit()

rezult_dosierujo = "json_datumoj/vortoj_frazoj_{}_{}.json".format(al, de)

def klaku_DIV_kun_titolo(tag, teksto):
    for e in driver.find_elements_by_tag_name(tag):
        dtext = e.text
        #print(e.text, "dtext='", dtext.lower(), "' teksto= '", teksto.lower(), "'", dtext.lower() == teksto.lower())
        if dtext.lower() == teksto.lower():
            e.click()
            return True
    print("Ne puŝis ion kun '{}'... :(".format(teksto))
    return False

driver = webdriver.Chrome()
driver.get("https://{}.duolingo.com/".format(de))

#lingvoelekto
print("Elektu vian dezerota elcxerplingvo, poste alklaku [Enter]...")
input()

#sen konto
button = driver.find_elements_by_tag_name("button")[-1]
driver.execute_script("arguments[0].click();", button)
time.sleep(.5)
#"learning goal"
button = driver.find_elements_by_tag_name("button")[-1]
driver.execute_script("arguments[0].click();", button)
time.sleep(.5)
#mi ne konas esperanton
button = driver.find_elements_by_tag_name("button")[-2]
driver.execute_script("arguments[0].click();", button)
time.sleep(.5)

#au uzu cxiuj en-eo vortoj, aux cxiuj vortoj de la lingvo
if cxiuj_vortoj:
    lekcioj_vortoj = json.loads(open("json_datumoj/duolingo_multilingva_vortaroj.json").read())[al]
else:
    lekcioj_vortoj = json.loads(open("json_datumoj/en_eo_vortoj.json").read())

# por ne plu elsxerpi la jam sxerpitaj
lekcioj_vortoj_cache = json.loads(open(rezult_dosierujo).read()) if os.path.isfile(rezult_dosierujo) else []
lekcioj_vortoj_cache_dict = {v["vorto"]:v for v in lekcioj_vortoj_cache}
lekcioj_vortaro_cache = [v["vorto"] for v in lekcioj_vortoj_cache]

vortoj = []
lekcioj_vortoj_novaj = []
for i, v in enumerate(lekcioj_vortoj):
    if v in lekcioj_vortaro_cache:
        vortoj += [lekcioj_vortoj_cache_dict[v]]
    else:
        lekcioj_vortoj_novaj += [v]

for i, v in enumerate(lekcioj_vortoj_novaj):
    try:
        driver.get("https://www.duolingo.com/dictionary")
        time.sleep(1)
        input = driver.find_element_by_tag_name("input")
        time.sleep(1)
        input.send_keys(v);
        time.sleep(1)
        button = driver.find_elements_by_tag_name("button")[0]
        driver.execute_script("arguments[0].click();", button)
        #klaku por vidi cxiuj frzojn
        time.sleep(3)
        help_tabeloj_links = [s.get_property("src") for s in driver.find_elements_by_tag_name("script") if "duolingo" in s.get_property("src")]
        #se havas helptabuloj certe havas vortojn
        retries = max([len(help_tabeloj_links), 2])
        for j in range(retries):
            try:
                klaku_DIV_kun_titolo("span", pli)
                break
            except Exception as e:
                time.sleep(1)
        korektita_vorto = driver.find_element_by_xpath("/html/body/div[1]/div/div[2]/div/div[2]/div[1]/h1").text
        traduko_de_vorto = driver.find_element_by_xpath(
            "/html/body/div[1]/div/div[2]/div/div[2]/div/div[1]/div/div/div[2]"
        ).text
        frazoj = []
        for j in range(100):
            try:
                frazo_eo = driver.find_element_by_xpath(
                            "/html/body/div[1]/div/div[2]/div/div[2]/div/div[1]/div/div/div[3]/div/div[{}]/div[2]/div[1]".format(j+1)
                           ).text.replace("\n", " ")
                frazo_traduko = driver.find_element_by_xpath(
                            "/html/body/div[1]/div/div[2]/div/div[2]/div/div[1]/div/div/div[3]/div/div[{}]/div[2]/div[2]".format(j+1)
                           ).text
                frazoj += [[frazo_eo, frazo_traduko]]
            except Exception as e:
               print("frazoj por vorto '{}' I=".format(v), j)
               break
        help_tabeloj = {}
        for link in help_tabeloj_links:
            driver.get(link)
            help_tabeloj[link.split("sentence=")[1]] = driver.find_element_by_tag_name("pre").text
        vortoj += [{"vorto": v, "korektita_vorto":korektita_vorto, "traduko": traduko_de_vorto, "frazoj": frazoj, "help_tabeloj": help_tabeloj}]
        print(i,"/",len(lekcioj_vortoj_novaj))
    except Exception as e:
        print("Fiaskis kun '{}'".format(v))
        pass

#konservi la frazaro
open(rezult_dosierujo, "w").write(json.dumps(vortoj, indent=4, ensure_ascii=False))
driver.close()
