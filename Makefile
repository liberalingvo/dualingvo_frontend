.PHONY: dualingvo
default: dualingvo ;

tmp/sentences.tar.bz2:
	wget http://downloads.tatoeba.org/exports/sentences.tar.bz2 -O tmp/sentences.tar.bz2
tmp/links.tar.bz2:
	wget http://downloads.tatoeba.org/exports/links.tar.bz2 -O tmp/links.tar.bz2
tmp/tags.tar.bz2:
	wget http://downloads.tatoeba.org/exports/tags.tar.bz2 -O tmp/tags.tar.bz2
tmp/sentences.csv: tmp/sentences.tar.bz2
	tar jxf tmp/sentences.tar.bz2 -C tmp
	touch tmp/sentences.csv
tmp/links.csv: tmp/links.tar.bz2
	tar jxf tmp/links.tar.bz2 -C tmp
	touch tmp/links.csv
tmp/eo_links.json: tmp/links.csv tmp/eo_links.csv
	python helpiloj/links_to_json.py
tmp/eo_sentences.csv: tmp/sentences.csv
	cat tmp/sentences.csv | grep -P "\tepo\t" | awk '{print $1;}' > tmp/eo_sentences.csv
tmp/eo_links.csv: tmp/links.csv tmp/eo_sentences.csv
	cut -f 1 tmp/eo_sentences.csv > tmp/eo_links.csv
tmp/tags.csv: tmp/tags.tar.bz2
	tar jxf tmp/tags.tar.bz2 -C tmp
	touch tmp/tags.csv
tmp/eo_frazoj.json: tmp/sentences.csv tmp/eo_links.json tmp/tags.csv
	python helpiloj/sentences_to_json.py
tmp/eo_enfrge.json: tmp/eo_frazoj.json
	python helpiloj/extract_most_contributed_languages.py

tmp/elparolontaj.json: tmp/eo_frazoj.json
	node eopl_transskribilo.js > tmp/elparolontaj.json
	mkdir -p static/mp3/
	python frazoj_tts_eopl.py

dualingvo: tmp/eo_enfrge.json tmp/elparolontaj.json
	npm install https://github.com/martinrue/eopl/tarball/master
	pip install -r requirements.txt --no-index

	cd static/ && npm install && cd ..

	mkdir -p leckcioj

	node eopl_transskribilo.js > frazojn.json
	python frazoj_tts_eopl.py
	python dua_lingo_app.py freeze
