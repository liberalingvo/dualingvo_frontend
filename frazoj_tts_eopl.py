from gtts import gTTS
import os, json

frazoj_transskribendaj = json.loads(open("tmp/elparolontaj.json").read())

for i, v in enumerate(frazoj_transskribendaj):
    filename = "static/mp3/" + str(v[1]) + ".mp3"
    if not os.path.isfile(filename): #nur ankoraux neekzistantaj
        print("Registras frazojn...", i, "/", len(frazoj_transskribendaj), "")
        tts = gTTS(text=v[0], lang='pl')
        tts.save(filename)
print("Registris ĉiujn frazojn.")
