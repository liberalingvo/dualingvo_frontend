import json

lingvoj = ["es", "en", "pt"]
tradukoj = ["vortoj_frazoj_{}.json".format(l) for l in lingvoj]
vortoj = json.loads(open("duolingo_webapp_esperanto_words.json").read())

multlingva = {v:{"tradukoj":{}, "frazoj":{}, "help_tabeloj":{}} for v in vortoj}
for l, t in zip(lingvoj, tradukoj):
    tradukoj = json.loads(open(t).read())
    for vorto in tradukoj:
        if not vorto["vorto"] in multlingva.keys(): continue
        v = multlingva[vorto["vorto"]]
        if len(vorto["frazoj"]):
            v["tradukoj"][l] = vorto["traduko"]
            v["frazoj"][l] = vorto["frazoj"]
            v["help_tabeloj"][l] = vorto["help_tabeloj"]

multlingva_kompleta = {m:multlingva[m] for m in multlingva if len(multlingva[m]["frazoj"])==len(lingvoj)}
print(len(multlingva_kompleta))
fr = open("vortoj_frazoj_multlingva_kompleta.json", "w").write(json.dumps(multlingva_kompleta, indent=4, ensure_ascii=False))

frazoj_eo_cxiuj = {}
for vorto, data in multlingva_kompleta.items():
    for l, frazoj in data["frazoj"].items():
        for f in frazoj:
            if f[0] not in frazoj_eo_cxiuj.keys():
                frazoj_eo_cxiuj[f[0]] = {}
                frazoj_eo_cxiuj[f[0]][l] = {}
            if l not in frazoj_eo_cxiuj[f[0]].keys():
                frazoj_eo_cxiuj[f[0]][l] = {}
            frazoj_eo_cxiuj[f[0]][l] = f[1]

for i, k in enumerate(frazoj_eo_cxiuj.keys()):
    frazoj_eo_cxiuj[k]["id"] = i

fr = open("vortoj_frazoj_multlingva_kompleta_ord.json", "w").write(json.dumps(frazoj_eo_cxiuj, indent=4, ensure_ascii=False))


"""
"vorto": "kie",
"traduko": "na qual, onde",
"frazoj": [
[
    "Kie en Eŭropo loĝas via amikino ?",
    "Onde na Europa mora a sua amiga?"
],
"help_tabeloj": {
"Kie%20vi%20lo%C4%9Das%3F": "/**
"""
