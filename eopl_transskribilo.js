var eopl = require('eopl');
var fs = require("fs");

var contents = fs.readFileSync("tmp/eo_frazoj.json");
var jsonContent = JSON.parse(contents);

var frazoj = [];
for(var frazo in jsonContent){
    frazoj.push([eopl.transcribe(frazo), jsonContent[frazo]["identigilo"]]);
}

console.log(
    JSON.stringify(frazoj)
);
