import json
import collections

fr = json.loads(open("tmp/eo_frazoj.json").read())
fr_lingvoj = []
enfrgel={}
for k,v in fr.items():
    notrans = 0
    langs = v.keys()
    if len(langs) == 0:
        notrans+=1
    lp = 0
    lp += 1 if "deu" in langs else 0
    lp += 1 if "fra" in langs else 0
    lp += 1 if "eng" in langs else 0
    if lp >= 3:
        fr_lingvoj += list(langs)
        enfrgel[k]=v

print(collections.Counter(fr_lingvoj))
print("notrans",notrans)
print("enfrge",len(enfrgel))
open("tmp/eo_enfrge.json","w").write(json.dumps(enfrgel, ensure_ascii=False, indent=4))

words = list([w.lower().replace(",","").replace("!","").replace("?","").replace(".","") for f in enfrgel.keys() for w in f.split()])
print(collections.Counter(words).most_common())
c = collections.Counter(words)
print("len(set(words))",len(set(words)))
