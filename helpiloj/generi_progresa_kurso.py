import json

duo_tree = json.loads(open("duo_tree_clean.json").read())

tatobea = json.loads(open("tmp/eo_enfrge.json").read())
lekcio = json.loads(open("json_datumoj/lekcioj.json").read())
tatoeba_eo = list(tatobea.keys())

unua_checkpoint = [
    "Introducing Yourself",
    "Common Phrases",
    "The Weather",
    "Plurals",
    "Everyday Life",
    "Accusative",
    "Possessives",
]

lernitaj_vortoj = ["Tom", "Mary" "Maria", "John", "Johannes", "Alice", "Elke", "Kate" ,"Katrin", "Jean", "Johanna", "Cookie","Krümel", "tomo", "tom", "Manjo", "Mary", "Jorge", "Ken"]
lernitaj_tatoeba_frazoj = []
lekcioj_vortoj_frazoj = {}
for l in unua_checkpoint:
    vortoj_frazoj = {}
    for sublekcio in lekcio[l]:
        for w in sublekcio:
            print(w)
            vortoj_frazoj[w] = []
            for f in tatoeba_eo:
                words = list(
                    [tw.lower().replace(",","").replace("!","").replace("?","").replace(".","")  for tw in f.split()]
                )
                if w in words:
                    vortoj_frazoj[w] += [f]
                    lernitaj_tatoeba_frazoj += [f]
                    print("\t", f)
    lekcioj_vortoj_frazoj[l] = vortoj_frazoj.copy()

vortoj_frazoj = open("tmp/vortoj_frazoj.json","w").write(json.dumps(lekcioj_vortoj_frazoj, ensure_ascii=False, indent=4))
print("len(lernitaj_tatoeba_frazoj)", len(set(lernitaj_tatoeba_frazoj)))
