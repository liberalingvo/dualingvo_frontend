import json

links = [[int(e) for e in l.split("\t") if e] for l in open("tmp/links.csv").read().split("\n")]
eo_links = [int(l) for l in open("tmp/eo_links.csv").read().split("\n") if l]
max_eo = max(eo_links)
is_eo_link = [False]*(max_eo+1)

for l in eo_links:
    is_eo_link[l] = True
linkedListed = {l[0]:[] for l in links if len(l) == 2 and max_eo >= l[0] and is_eo_link[l[0]]}

l1, l2 = len(linkedListed), len(is_eo_link)
for l in links:
    if len(l) != 2 or max_eo < l[0] or not is_eo_link[l[0]]:
        continue
    linkedListed[l[0]] += [int(l[1])]

linkedListed_no_empties = {k:v for k,v in linkedListed.items() if v}
open("tmp/eo_links.json","w").write(json.dumps(linkedListed_no_empties, ensure_ascii=False, indent=4))
