import json

fr = [t.split("\t") for t in open("sentences.csv").read().split("\n")]
frb = {int(f[0]):f[1:] for f in fr if f[0]}
links = json.loads(open("links.json").read())
tags = [t.split("\t") for t in open("tags.csv").read().split("\n")]
tags = {int(f[0]):f[1:] for f in tags if f[0]}

frr={}
failed = 0
for k,v in links.items():
    try:
        k=int(k)
        if frb[k][0]=="epo":
            frr[frb[k][1]] = {frb[int(vv)][0]:frb[int(vv)][1] for vv in v}
            frr[frb[k][1]]["tags"] = tasg[k] if k in tags else []
            frr[frb[k][1]]["identigilo"] = k
    except Exception as e:
        failed+=1
print("Failed {}/{}".format(failed,len(links.keys())))
open("tmp/eo_frazoj.json","w").write(json.dumps(frr, ensure_ascii=False, indent=4))
