# DuaLingvo

## Komenca fluo de lecionsintezo

![De grupitaj vortlisto al ekzempleraj frazoj en dualingva stilo kun aŭdio](doc/lekcifluo.png)

## TODOs

* por la aŭdio en la statika versio ne taŭgas la ligilojn... (ili ne estas relativaj sed de 'root')
* pripensu TODOs

## Dependoj

* inkscape
* node & npm
* python & pip
* selenium & Chrome

## Uzado

Por elŝuti/generi la datumojn `make`, por servi poste lokale `python dua_lingo_app.py`.

## Skrapi

`pip install requirements.txt`

Ekzemple por skrapi la anglan por nederlandparolantoj:

`python -i vortlist_cxerpilo.py --cellingvo en --gepatralingvo nl --pli_da_frazoj "Toon meer zinnen"`

Ekzemple por skrapi la nederlandan por angloparolantoj:

`python -i vortlist_cxerpilo.py --cellingvo nl --gepatralingvo en --pli_da_frazoj "Show More Sentences"`

Poste vie mem devas alklaki vian ligvon kaj cellingvon en duolingvo. Atendu poste, ĝis ŝargis la paĝo, kiu demandas pri via dezirata trejnofteco kaj alklaku [Enter] en la konzolo. La skripto bezonas inter 6 kaj 20 horojn por fini.

La `-i` opcio protektas vin kaze ke la skript ĉesas funkcion. Vi eniras aŭ pro eraro en la interaktiva konsolo, aŭ mem iru tien (se ekzemple retkonekto fiaskis) pere de STRG+C. Tie entajpu `open(rezult_dosierujo, "w").write(json.dumps(vortoj, indent=4, ensure_ascii=False))` por konservi la progreson. Restartu la skripton nun.

Normale ĝi nur elŝutas la vortojn, kiuj estas uzata en la en-eo kurso. Se vi volas elŝuti kompletan kurson vi aldonu la vortlisto de la dezirata lingvo al `duolingo_multilingva_vortaroj.json` kaj ekzekutu pere de `-a`.

Vi trovas la vortlisto de via lingvo, kiam vi vizitas duolingo, premas F12 en la retumilo kaj iras al 'Storage>Local Storage>Duolingo>duo.languageTokens.XX' kaj XX estas via lingvokodo ekzemple `eo`, `en`, `ru`...

## Dankoj

Dankon al https://game-icons.net/ por la bildojn!
