import datetime, flask, json, sys, jinja2, mistune, markdown
from flask import render_template, jsonify, request, abort, url_for
from flask_frozen import Freezer

md = markdown.Markdown(extensions=['meta'])
env = jinja2.Environment()
env.filters['markdown'] = lambda text: jinja2.Markup(md.convert(text))

app = flask.Flask(__name__)
freezer = Freezer(app)
app.config["FREEZER_IGNORE_404_NOT_FOUND"]=True
app.config["FREEZER_RELATIVE_URLS"]=True

lekcioj = json.loads(open("json_datumoj/lekcioj.json").read())

@app.route('/')
def hejme():
    unua_checkpoint = [
        ["Introducing Yourself"],
        ["Common Phrases", "The Weather"],
        ["Plurals"],
        ["Everyday Life"],
        ["Accusative", "Possessives"],
    ]
    icons = {
        "Introducing Yourself": "conversation.svg",
        "Common Phrases": "talk.svg",
        "The Weather": "sun-cloud.svg",
        "Plurals": "juggler.svg",
        "Everyday Life": "house.svg",
        "Accusative": "target-dummy.svg",
        "Possessives": "pay-money.svg",
    }
    return render_template('index.html', tree=unua_checkpoint, icons=icons, lekcioj=lekcioj, progress=[100,75,0,20])

@freezer.register_generator
def kurso():
    for lec in lekcioj.keys():
        yield {"lekcio_nomo":lec}
@app.route('/<string:lekcio_nomo>.html')
def kurso(lekcio_nomo):
    l = lekcioj[lekcio_nomo]
    frazoj = []
    for v in json.loads(open("vortoj_frazoj.json").read()):
        if v["vorto"] in l:
            frazoj += v["frazoj"]
    print(frazoj)
    return render_template('kurso.html', lekcio_nomo=lekcio_nomo, lekcio=lekcioj[lekcio_nomo], frazoj=frazoj)

if __name__ == '__main__':
    if "freeze" in sys.argv:
        freezer.freeze()
    else:
        app.config['DEBUG'] = True
        app.run()
